import docker
import time

client = docker.from_env()

#container = client.containers.run("collector/memory:latest",detach=True)
#container = client.containers.lost
container = client.containers.get('fb4182308871')
#container = client.containers.get("mnist-keras")
#logs = container.logs(stream=True)

print(container.logs(stream=True))
#for line in container.logs(stream=True):
#   print(line.strip())
def getLog(container):
   log = container.logs(stream=True)
   stop = False
   while not stop:
      for line in log:
         if 'collector:end' in  line.strip():
            stop = True
         print(line)
      if not stop:
         print('collector:sleep')
         time.sleep(2)
         log = container.logs(stream=True)
   
